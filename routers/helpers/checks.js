const {
  NoUserError,
  BadPasswordError,
  NoTokenError,
  NotYoursError
} = require('../../Errors');
const bcrypt = require('bcrypt');

module.exports.checkPassword = async (passOne, passTwo) => {
  if (await bcrypt.compare(passOne, passTwo)) return true;
  throw new BadPasswordError();
};

module.exports.checkToken = token => {
  if (token) return token;
  throw new NoTokenError();
};

module.exports.checkUser = user => {
  if (user) return user;
  throw new NoUserError();
};

module.exports.checkPassword = async (passOne, passTwo) => {
  if (await bcrypt.compare(passOne, passTwo)) return true;
  throw new BadPasswordError();
};

module.exports.isYours = (yourId, itemId) => {
  if (yourId.toString() === itemId.toString()) return true;
  throw new NotYoursError();
};
