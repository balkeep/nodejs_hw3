const bcrypt = require('bcrypt');
const { User } = require('../../models/UserModel');
const { Truck } = require('../../models/TruckModel');
const { Load } = require('../../models/LoadModel');
const {
  checkUser,
  checkPassword,
  isYours
} = require('../../routers/helpers/checks');
const { validateUser } = require('../../routers/helpers/validation');
const {
  WrongEntityIdError,
  NonExistantTypeError,
  NoTruckAssignedError,
  UnfittableLoadError,
  OnlyPostingNewLoadsError,
  NoActiveLoadError,
  LoadArchivedError
} = require('../../Errors');
const { TRUCK_TYPES, LOAD_STATES } = require('../../config');

const getItemFromDb = async (type, id) => {
  try {
    let res;
    if (type === 'truck') {
      res = await Truck.findById(id);
    } else if (type === 'load') {
      res = await Load.findById(id);
    }
    if (!res) throw new WrongEntityIdError();
    return res;
  } catch (e) {
    throw new WrongEntityIdError();
  }
};

const unassignTrucks = async id => {
  const assignedTucks = await Truck.find({ created_by: id, assigned_to: id });
  assignedTucks.forEach(async truck => {
    truck.assigned_to = '';
    await truck.save();
  });
};

const fittingTypes = load => {
  let smallestFittingTypeIndex = null;
  for (let i = 0; i < TRUCK_TYPES.length && !smallestFittingTypeIndex; i++) {
    const type = TRUCK_TYPES[i];
    if (
      type.payload >= load.payload &&
      type.width >= load.width &&
      type.length >= load.length &&
      type.height >= load.height
    ) {
      smallestFittingTypeIndex = i;
    }
  }
  const typesThatFit = TRUCK_TYPES.slice(
    smallestFittingTypeIndex,
    TRUCK_TYPES.length
  ).map(({ type }) => type);
  if (typesThatFit.length === 0) throw new UnfittableLoadError();
  return typesThatFit;
};

const findFittingTruck = async load => {
  const types = fittingTypes(load);

  let truck = null;
  for (let i = 0; i < types.length && !truck; i++) {
    const type = types[i];
    truck = await Truck.findOne({ type, status: 'IS' })
      .where('assigned_to')
      .ne(null);
  }
  return truck;
};

const logLoad = (load, message) =>
  load.logs.push({ message, time: Date.now() });

module.exports.createUser = async ({ email, role, password }) => {
  const user = new User({
    email,
    role,
    password: await bcrypt.hash(password, 10)
  });

  await user.save();
};

module.exports.getUserByEmail = async email =>
  checkUser(await User.findOne({ email }));

module.exports.getUserById = async id => checkUser(await User.findById(id));

module.exports.rmUser = async id => {
  const user = checkUser(await User.findById(id));
  await user.remove();
};

module.exports.updatePasswd = async (id, oldPassword, newPassword) => {
  const user = checkUser(await User.findById(id));
  await checkPassword(oldPassword, user.password);
  await validateUser({
    email: user.email,
    role: user.role,
    password: newPassword
  });
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();
};

module.exports.getItems = async ({ type, status, created_by, skip, limit }) => {
  if (type === 'truck') {
    return await Truck.find({ created_by }, null, { skip, limit });
  } else if (type === 'load') {
    if (status) {
      return await Load.find({ created_by, status }, null, { skip, limit });
    } else {
      return await Load.find({ created_by }, null, { skip, limit });
    }
  }
  throw new Error();
};

module.exports.addItem = async (type, item) => {
  if (type === 'truck') {
    await new Truck(item).save();
  } else if (type === 'load') {
    await new Load(item).save();
  } else {
    throw new NonExistantTypeError();
  }
};

module.exports.getItemById = async (type, id, userId) => {
  const item = await getItemFromDb(type, id);
  isYours(userId, item.created_by);

  return item;
};

module.exports.getTruckAssignedToUser = async userId => {
  if (!userId) throw new NoTruckAssignedError();
  return await Truck.findOne({ assigned_to: userId });
};

module.exports.updItemById = async (type, itemId, userId, payload) => {
  const item = await getItemFromDb(type, itemId);

  isYours(userId, item.created_by);
  const payloadKeys = Object.keys(payload);
  for (let key of payloadKeys) {
    item[key] = payload[key];
  }

  await item.save();
};

module.exports.rmItemById = async (type, itemId, userId) => {
  const item = await getItemFromDb(type, itemId);
  isYours(userId, item.created_by);
  item.remove();
};

module.exports.assignTruck = async (truckId, userId) => {
  const truck = await getItemFromDb('truck', truckId);
  isYours(userId, truck.created_by);
  truck.assigned_to = userId;

  await unassignTrucks(userId);
  await truck.save();
};

module.exports.postUsersLoadById = async (loadId, userId) => {
  const load = await getItemFromDb('load', loadId);
  isYours(userId, load.created_by);
  if (load.status !== 'NEW') throw new OnlyPostingNewLoadsError();

  load.status = 'POSTED';
  logLoad(load, 'Load posted');
  await load.save();

  const truck = await findFittingTruck(load);
  if (truck) {
    load.status = 'ASSIGNED';
    load.state = LOAD_STATES[0];
    load.assigned_to = truck.assigned_to;
    logLoad(load, `Load assigned to a driver with id ${truck.assigned_to}`);
    await load.save();
    console.log(truck);
    await Truck.findByIdAndUpdate(truck._id, { status: 'OL' });
    return { message: 'Load posted successfully', driver_found: true };
  } else {
    load.status = 'NEW';
    logLoad(load, `Driver search failed, load reverted to NEW status`);
    await load.save();
    return { message: 'Load post attempt failed', driver_found: false };
  }
};

module.exports.getActiveLoadsOfTheUser = async userId =>
  await Load.where('state')
    .ne(null)
    .ne(LOAD_STATES[LOAD_STATES.length])
    .findOne({ assigned_to: userId });

module.exports.loadProgress = async userId => {
  const load = await Load.where('state')
    .ne(null)
    .ne(LOAD_STATES[LOAD_STATES.length])
    .findOne({ assigned_to: userId });
  let newState = null;

  if (!load) throw new NoActiveLoadError();
  const currStatusIndex = LOAD_STATES.findIndex(state => state === load.state);
  if (currStatusIndex === LOAD_STATES.length - 2) {
    load.status = 'SHIPPED';
  }
  if (currStatusIndex <= LOAD_STATES.length - 2) {
    newState = LOAD_STATES[currStatusIndex + 1];
    load.state = newState;
  } else {
    throw new LoadArchivedError();
  }
  await load.save();
  return newState;
};
