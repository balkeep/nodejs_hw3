const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../../config');
const { InvalidTokenError } = require('../../Errors');

module.exports.getToken = userData => jwt.sign(userData, JWT_SECRET);

module.exports.getDataFromToken = token => {
  try {
    return jwt.verify(token, JWT_SECRET);
  } catch (e) {
    throw new InvalidTokenError();
  }
};
