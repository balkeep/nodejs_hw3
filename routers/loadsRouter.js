const express = require('express');
const router = new express.Router();
const { asyncWrapper: aW } = require('./helpers/asyncWrapper');
const { authMiddleware } = require('../routers/middlewares/authMiddleware');
const {
  dbAddLoad,
  dbGetMyLoads,
  dbGetLoadById,
  dbUpdLoadById,
  dbRmLoadById,
  dbGetShippingInfo,
  dbPostALoadById,
  dbGetMyActiveLoads,
  dbLoadProgress
} = require('../controllers/loadsController');

router.post('/', aW(authMiddleware), aW(dbAddLoad));
router.get('/', aW(authMiddleware), aW(dbGetMyLoads));
router.get('/active', aW(authMiddleware), aW(dbGetMyActiveLoads));
router.patch('/active/state', aW(authMiddleware), aW(dbLoadProgress));
router.get('/:id', aW(authMiddleware), aW(dbGetLoadById));
router.put('/:id', aW(authMiddleware), aW(dbUpdLoadById));
router.delete('/:id', aW(authMiddleware), aW(dbRmLoadById));
router.post('/:id/post', aW(authMiddleware), aW(dbPostALoadById));
router.get('/:id/shipping_info', aW(authMiddleware), aW(dbGetShippingInfo));

module.exports = router;
