const express = require('express');
const router = new express.Router();

const { asyncWrapper } = require('./helpers/asyncWrapper');
const { login, register, iforgot } = require('../controllers/authController');

router.post('/register', asyncWrapper(register));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(iforgot));

module.exports = router;
