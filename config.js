module.exports = {
  PORT: process.env.PORT || 8080,
  JWT_SECRET: process.env.JWT_SECRET || 'pWHyNhPowBc8wNZzwMu7',
  DB_USER: process.env.DB_USER || 'leo',
  DB_PASS: process.env.DB_PASS || 'qqqq',
  DB_NAME: process.env.DB_NAME || 'nodejs_hw3',
  DB_HOSTNAME: process.env.DB_HOSTNAME || 'cluster0.e5tnm.mongodb.net',
  TRUCK_TYPES: [
    {
      type: 'SPRINTER',
      payload: 1700,
      dimensions: { width: 300, length: 250, height: 170 }
    },
    {
      type: 'SMALL STRAIGHT',
      payload: 2500,
      dimensions: { width: 500, length: 250, height: 170 }
    },
    {
      type: 'LARGE STRAIGHT',
      payload: 4000,
      dimensions: { width: 700, length: 350, height: 200 }
    }
  ],
  LOAD_STATES: [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'
  ]
};
