require('dotenv').config();
const morgan = require('morgan');
const express = require('express');
const app = express();

const { PORT } = require('./config');
const { startDB } = require('./dbConnector');

const { errorHandler } = require('./errorHandler');
const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');

app.use(morgan('dev'));
app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

app.use(errorHandler);

startDB();
app.listen(PORT, () => {
  console.log(`Server has started at port ${PORT}!`);
});
