const parseMongoError = (e, req, res, next) => {
  if (e.code === 11000) {
    const { email } = req.body;
    res
      .status(400)
      .json({ message: `User with email: ${email} already exists!` });
  } else {
    res.status(400).json({ message: 'Incorrect request (DB Error)' });
  }
};

module.exports.errorHandler = (e, req, res, next) => {
  if (e.statusCode) {
    res.status(e.statusCode).json({ message: e.message });
  } else if (e.name === 'ValidationError') {
    res.status(400).json({ message: e.details[0].message });
  } else if (e.name === 'MongoError') {
    parseMongoError(e, req, res, next);
  } else {
    console.log(`*** ${e.name}: ${e.message} ***`);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};
