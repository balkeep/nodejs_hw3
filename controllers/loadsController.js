const {
  validateLoad,
  validateSkipLimit
} = require('../routers/helpers/validation');
const {
  addItem,
  getItems,
  getItemById,
  updItemById,
  rmItemById,
  getTruckAssignedToUser,
  postUsersLoadById,
  getActiveLoadsOfTheUser,
  loadProgress
} = require('../routers/helpers/dbHelpers');

module.exports.dbAddLoad = async (req, res) => {
  const { _id } = res.locals;
  let load = req.body;

  await validateLoad(load);

  load = {
    created_by: _id,
    ...load
  };

  await addItem('load', load);

  res.json({ message: 'Load added successfully' });
};

module.exports.dbGetMyLoads = async (req, res) => {
  const { status, limit, offset: skip } = req.query;
  await validateSkipLimit({ limit, skip });

  const { _id: created_by } = res.locals;
  const loads = await getItems({
    type: 'load',
    created_by,
    status,
    limit: +limit,
    skip: +skip
  });

  res.json({ loads });
};

module.exports.dbGetLoadById = async (req, res) => {
  const { id } = req.params;
  const { _id } = res.locals;

  res.json({ load: await getItemById('load', id, _id) });
};

module.exports.dbUpdLoadById = async (req, res) => {
  const { id: loadId } = req.params;

  const load = req.body;
  await validateLoad(load);

  const { _id: userId } = res.locals;
  await updItemById('load', loadId, userId, load);

  res.json({ message: 'Load changed successfully!' });
};

module.exports.dbRmLoadById = async (req, res) => {
  const { id } = req.params;
  const { _id } = res.locals;

  await rmItemById('load', id, _id);

  res.json({ message: 'Load removed successfully!' });
};

module.exports.dbGetShippingInfo = async (req, res) => {
  const { id } = req.params;
  const { _id } = res.locals;

  const load = await getItemById('load', id, _id);
  const truck = await getTruckAssignedToUser(load.assigned_to);

  res.json({ load, truck });
};

module.exports.dbPostALoadById = async (req, res) => {
  const { id: loadId } = req.params;
  const { _id: userId } = res.locals;

  res.json(await postUsersLoadById(loadId, userId));
};

module.exports.dbGetMyActiveLoads = async (req, res) => {
  const { _id: userId } = res.locals;

  const load = await getActiveLoadsOfTheUser(userId);

  res.json({ load });
};

module.exports.dbLoadProgress = async (req, res) => {
  const { _id: userId } = res.locals;

  const newStatus = await loadProgress(userId);
  res.json({ message: `Load state changed to '${newStatus}'` });
};
