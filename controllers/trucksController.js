const {
  validateSkipLimit,
  validateTruck
} = require('../routers/helpers/validation');
const {
  getItems,
  addItem,
  getItemById,
  updItemById,
  rmItemById,
  assignTruck
} = require('../routers/helpers/dbHelpers');

module.exports.dbGetTrucks = async (req, res) => {
  const { limit, offset: skip } = req.query;
  await validateSkipLimit({ limit, skip });

  const { _id: created_by } = res.locals;
  const trucks = await getItems({
    type: 'truck',
    created_by,
    limit: +limit,
    skip: +skip
  });

  res.json({ trucks });
};

module.exports.dbAddTruck = async (req, res) => {
  const { _id: created_by } = res.locals;
  let truck = req.body;

  await validateTruck(truck);

  truck = {
    created_by,
    ...truck
  };

  await addItem('truck', truck);

  res.json({ message: 'Truck added successfully' });
};

module.exports.dbGetTruckById = async (req, res) => {
  const { id } = req.params;
  const { _id } = res.locals;

  res.json({ truck: await getItemById('truck', id, _id) });
};

module.exports.dbUpdTruckById = async (req, res) => {
  const { id: truckId } = req.params;

  const truck = req.body;
  await validateTruck(truck);

  const { _id: userId } = res.locals;
  await updItemById('truck', truckId, userId, truck);

  res.json({ message: 'Truck changed successfully!' });
};

module.exports.dbDelTruckById = async (req, res) => {
  const { id } = req.params;
  const { _id } = res.locals;

  await rmItemById('truck', id, _id);

  res.json({ message: 'Truck removed successfully!' });
};

module.exports.dbAssignTruck = async (req, res) => {
  const { id } = req.params;
  const { _id } = res.locals;

  await assignTruck(id, _id);

  res.json({ message: 'Truck assigned successfully!' });
};
