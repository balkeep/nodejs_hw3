const { validateUser } = require('../routers/helpers/validation');
const { createUser, getUserByEmail } = require('../routers/helpers/dbHelpers');
const { checkPassword } = require('../routers/helpers/checks');
const { getToken } = require('../routers/helpers/authToken');

module.exports.register = async (req, res) => {
  const user = req.body;

  await validateUser(user);
  await createUser(user);

  res.json({ message: 'User created successfully!' });
};

module.exports.login = async (req, res) => {
  const { email, password } = req.body;

  const user = await getUserByEmail(email);
  await checkPassword(password, user.password);

  res.json({
    message: 'Success',
    jwt_token: getToken({ _id: user._id, email: user.email, role: user.role })
  });
};

module.exports.iforgot = async (req, res) =>
  res.json({
    message: "If your email is registered, we've sent uou a new password"
  });
